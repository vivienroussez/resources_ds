#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(plotly)
#library(dygraph)

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
   
  output$distPlot <- renderPlot({
    
    # generate bins based on input$bins from ui.R
    x    <- faithful[, input$vari] 
    bins <- seq(min(x), max(x), length.out = input$bins + 1)
    
    # draw the histogram with the specified number of bins
    # hist(x, breaks = bins,
    #      border = 'white',main=input$titre,col=input$coul)
    g <- ggplot(faithful,aes_string(x=input$vari)) + 
          geom_histogram(bins = input$bins,fill=input$coul) +
          ggtitle(input$titre)
    g
    })
  output$affiche <- renderPrint(paste("Le nombre de bins est",input$bins))
  output$donnees <- renderDataTable(faithful)
  output$table <- renderPrint(summary(faithful))
  output$control <- renderPrint(input$vari)
  
})
