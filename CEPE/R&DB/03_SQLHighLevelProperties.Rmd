---
title: "SQL High Level properties"
output:
  html_document:
    highlight: pygments
---

<link rel="stylesheet" href="http://yandex.st/highlightjs/7.3/styles/default.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<script>
$(document).ready(function() {
  $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
});
</script>

## SQL Characteristics

* Data stored in columns and tables
* Relationships represented by data
* Data Manipulation Language
* Data Definition Language 
* Transactions
* Abstraction from physical layer


## SQL provides

* Store persistent data
* Storing large amounts of data  on disk, while allowing  applications to grab the bits  they need through queries
* Application Integration
* Many applications in an enterprise need to share information. By getting all applications to use the database, we ensure all these applications have consistent, up-to-date data
* Mostly Standard
* The relational model is widely used and understood. Interaction with the database is done with SQL, which is a (mostly) standard language. This degree of standardization is enough to keep things familiar so people don’t need to learn new things


## Physical Layer Abstraction

* Applications specify what, not how
* Query optimization engine
* Physical layer can change without modifying applications
* Create indexes to support queries

## Data Manipulation Language (DML)

* Data manipulated with Select, Insert, Update, & Delete statements
* Select T1.Column1, T2.Column2 … From Table1, Table2 …
Where T1.Column1 = T2.Column1 …
* Data Aggregation
* Compound statements
* Functions and Procedures
* Explicit transaction control


## Data Definition Language (DDL)

* Schema defined at the start
* Create Table (Column1 Datatype1, Column2 Datatype 2, …)
* Constraints to define and enforce relationships
* Primary Key
* Foreign Key
* Etc.
* Triggers to respond to Insert, Update , & Delete
* Stored Modules
* Alter …
* Drop …
* Security and Access Control

## Transactions – ACID Properties

* Atomic – All of the work in a transaction completes (commit) or none of it completes
* Consistent – A transaction transforms the database from one consistent state to another consistent state. Consistency is defined in terms of constraints.
* Isolated – The results of any changes made during a transaction are not visible until the transaction has committed.
* Durable – The results of a committed transaction survive failures

## Database Schema 

* Structure described in a formal language (SQL) supported by the database management system (DBMS). 
* It refers to the organization of how the database is constructed (divided into database tables and the constraints between the tables).
* Data Normalisation 
