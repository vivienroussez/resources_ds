---
title: "Lahman"
output: html_document
---
Explore the change in player salaries and team payrolls over time via Sean Lahman’s baseball data.

In data/lahman591-csv you may find the csv file for the 2015 seanlahman baseball database. 

http://www.seanlahman.com/baseball-archive/statistics/

Use csv file to answer the following questions

#### Retrieve the payrolls for all teams and years (Salaries.csv)


#### Plot Boxplots of Team Payrolls by League


#### Add Team Names to the Payroll Data


####Add World Series Records to the Payroll Data


## Misc

* Which team lost the World Series each year? 
* Do these teams also have high payrolls?
* Some argue that teams with lower payrolls make it into the post season playoffs
* Augment the team payrolls to include each team’s name, division, and league. Create
a visualization that includes this additional information.
* Do veteran players have high salaries near the end of their careers.
Among players in the database How many are players? How many are managers? How many are both, or neither?

* Do pitchers get better with age? Is there an improvement and then a fall off in
performance?
