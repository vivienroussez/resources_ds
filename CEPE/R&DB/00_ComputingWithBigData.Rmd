---
title: "Computing with Big data"
output:
  html_document:
    highlight: pygments
---

<link rel="stylesheet" href="http://yandex.st/highlightjs/7.3/styles/default.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<script>
$(document).ready(function() {
  $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
});
</script>

## Computing with Big data

![hadoop1](img/hadoop1.png)


### Our problems : 

* Store Access and Filter Data

* Aggregate (run computations on ) data

### Two approaches :

Business  : Cobol and RDBMS 

Scientific : Unix, C and S/R 

* IBM, AT&T

![hadoop1](img/70s.jpg)


http://www.scaruffi.com/science/elec9.html


## Computing 


![hadoop22] <img src="img/boulier.jpg?raw=true" style="width: 50%;"/>




### Traditional Large-Scale Computation

* Relatively small amounts of data
* Significant amount of complex processing performed on that data
* For decades, the primary push was to increase the computing power of a single machine
* Faster processor, more RAM  : Vertical Scalability


### High‐performance (HPC)  and Grid Computing : Vertical Scalability/concentration

Super-computers evolution : 


![hadoop3](img/History_of_Computing.png)


The biggest : Sunway TaihuLight

![hadoop2](img/TaihuLight.jpg)

* 40,960 SW26010 manycore 64-bit RISC processors (> 1M cores)
* Cost 	1.8 billion Yuan (US$273 million)




### What is a Cluster?  

Cluster is a group of machines interconnected in a way that they work together as a single system 

#### Terminology 

* Node – individual machine in a cluster 
* Head/Master node 
    + connected to both the private network of the cluster and a public network and are used to access a given cluster. 
    + Responsible for providing user an environment to work and distributing task among other nodes 
* Compute nodes 
    + connected to only the private network of the cluster and are generally used for running jobs assigned to them by the head node(s) 

![hadoop22] <img src="img/cluster.jpg?raw=true" style="width: 30%;"/>


#### Types of Cluster 

* Storage Storage clusters provide a consistent file system image Allowing simultaneous read and write to a single shared file system 

* High‐availability (HA) Provide continuous availability of services by eliminating single points of failure 

* Load‐balancing Sends network service requests to multiple cluster nodes to balance the requested load among the cluster nodes 

* High‐performance Use cluster nodes to perform concurrent calculations Allows applications to work in parallel to enhance the performance of the applications Also referred to as computational clusters or grid computing


### Grid computing 
![hadoop2](img/grid-computing.jpg)


* Cpu Work is shared among cpu resources
* Grid data is not specified (ad hoc, ec gigaspaces)
* Data may be moved across servers
* Limit : Data volume : 

Does not deal with the management of huge volumes of data related their computations

## Big data ecosystem birth

![hadoop2](img/hadoop2.png)


