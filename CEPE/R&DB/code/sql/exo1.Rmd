---
title: "SQL exercise"
output: html_document
---

http://sqlzoo.net/wiki/White_Christmas
http://www.metoffice.gov.uk/hadobs/hadcet/data/download.html

Ues this vector for columns :  

```{r}

c('yr' ,'dy' ,'m1' ,'m2' ,'m3' ,'m4' ,'m5' ,'m6' ,'m7' ,'m8' ,'m9' ,'m10','m11','m12')


```


The units are 10th of a degree Celcius. The columns are yr and dy for year and day of month. The next twelve columns are for January through to December. 

* Show the average daily temperature for August 10th 1964
* Show the temperature on Christmas day (25th December) between 1812 and 1824
* Show which years where a White Christmas (if there was a day with an average temperature below zero between 21st and 25th of December)
* How many White Christmases have you seen ? 
* Climate Change ; Show the average temperatures for August by decade