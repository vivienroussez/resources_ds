---
title: "Performance"
output: slidy_presentation
---

```{r}
require(microbenchmark)
```


# Microbenchmarking

```{r}

library(microbenchmark)

x <- runif(100)
microbenchmark(
  sqrt(x),
  x ^ 0.5
)

```


Currently R-core has twenty members, but only six are active in day-to-day development. No one on R-core works full time on R. 

The overriding concern for R-core is not to make R fast, but to build a stable platform for data analysis and statistics



# huge relative variation in performance :

Extracting a single value from a data frame : 

```{r, echo=T, eval=F}
microbenchmark(
  "[32, 11]"      = mtcars[32, 11],
  "$carb[32]"     = mtcars$carb[32],
  "[[c(11, 32)]]" = mtcars[[c(11, 32)]],
  "[[11]][32]"    = mtcars[[11]][32],
  ".subset2"      = .subset2(mtcars, 11)[32]
)
```




# huge relative variation in performance :

Extracting a single value from a data frame : 

```{r, echo=T}
microbenchmark(
  "[32, 11]"      = mtcars[32, 11],
  "$carb[32]"     = mtcars$carb[32],
  "[[c(11, 32)]]" = mtcars[[c(11, 32)]],
  "[[11]][32]"    = mtcars[[11]][32],
  ".subset2"      = .subset2(mtcars, 11)[32]
)
```


# squish : 

A function that ensures that the smallest value in a vector is at least a and its largest value is at most b.

```{r}

require(scales)
squish(c(-1, 0.5, 1, 2, NA))
squish(c(-1, 0, 0.5, 1, 2), range = c(-1, 1))
```

# Other implementations

```{r}
squish_ife <- function(x, a, b) {
  ifelse(x <= a, a, ifelse(x >= b, b, x))
}
squish_p <- function(x, a, b) {
  pmax(pmin(x, b), a)
}
squish_in_place <- function(x, a, b) {
  x[x <= a] <- a
  x[x >= b] <- b
  x
}

x <- runif(100, -1.5, 1.5)
microbenchmark(
  squish_ife      = squish_ife(x, -1, 1),
  squish_p        = squish_p(x, -1, 1),
  squish_in_place = squish_in_place(x, -1, 1),
  unit = "us"
)

```

Look for built in function replacement for performance issues


# Rcpp

# Getting started

```{r}
library(Rcpp)

cppFunction('int add(int x, int y, int z) {
  int sum = x + y + z;
  return sum;
}')
# add works like a regular R function
add
#> function (x, y, z) 
#> .Primitive(".Call")(<pointer: 0x7f2f4aa933d0>, x, y, z)
add(1, 2, 3)

```



# squish cpp
```{r}
sourceCpp("cpp/squish_cpp.cpp")

microbenchmark(
  squish_in_place = squish_in_place(x, -1, 1),
  squish_cpp      = squish_cpp(x, -1, 1),
  unit = "us"
)

```


# Gibbs sampler

```{r}

gibbs_r <- function(N, thin) {
  mat <- matrix(nrow = N, ncol = 2)
  x <- y <- 0

  for (i in 1:N) {
    for (j in 1:thin) {
      x <- rgamma(1, 3, y * y + 4)
      y <- rnorm(1, 1 / (x + 1), 1 / sqrt(2 * (x + 1)))
    }
    mat[i, ] <- c(x, y)
  }
  mat
}

```

# cpp version

```{r}

sourceCpp("cpp/gibbs_cpp.cpp")

microbenchmark(
  gibbs_r(100, 10),
  gibbs_cpp(100, 10)
)

```

http://dirk.eddelbuettel.com/papers/rcpp_sydney-rug_jul2013.pdf


# Evaluating the Design of the R Language

http://r.cs.purdue.edu/pub/ecoop12.pdf




