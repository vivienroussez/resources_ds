require(foreign)
require(tidyverse)
require(FactoMineR)
require(leaps)  ## Pour la sélection de modèles. Mais bestglm est mieux (plus simple pour trouver le modèle final)
require(rpart)
require(ROCR)

setwd("/Volumes/USB/Datascience/ML1/")
par(mfrow=c(1,1))

#### Exercice 1

dat <- read.table("tp2.dta")
pairs(formula=maxO3 ~ .,data=dat)


# Coup d'oeil aux données
PCA(dat[,-1])

fit <- lm(formula=maxO3 ~ .,data=select(dat,-heure))
summary(fit)
plot(fit)

fit <- lm(formula=maxO3 ~ .^3,data=select(dat,-heure)) #### Introduit toutes les intéractions 
                                                       #### Entre toutes les variables
summary(fit)

mesures <- influence.measures(fit) # Calcule les différentes mesures d'influence
colnames(mesures$infmat)
plot(mesures$infmat[,"hat"],type="h")   # Points leviers (hat matrix)
rstudent(fit) %>% plot()
abline(h=c(-2,0,2),col="blue",lty=2)
cooks.distance(fit) %>% plot()

# Model sélection
lm(formula=maxO3 ~ . -Ne12,data = dat) %>% summary()
step(fit,direction = "backward")

sel <- regsubsets(y=dat$maxO3,x=select(dat,-maxO3,-heure),method = "exhaustive")
summary(sel)
plot(sel,scale="adjr2")





#### Exercice 2
rm(list=ls())
dat <- read.table("tp4.dta",sep=";",h=T)
pairs(dat)
fit <- lm(data=dat,formula = Y~.)
resp <- residuals(fit,type="partial")

par(mfrow=c(2,2))
for (i in 1:4) plot(dat[,i],resp[,i])   # On représetnte les résidus partiels en fonction de 
                                        # la variable à laquelle ils correspondent

# On refait la régression en transformant la variable
test <- mutate(dat,X5=X4^2) %>% select(-X4)
fit <- lm(data=test,formula = Y~. )
resp <- residuals(fit,type="partial")

par(mfrow=c(2,2))
for (i in 1:4) plot(test[,i],resp[,i])   # On représetnte les résidus partiels en fonction de 
# la variable à laquelle ils correspondent

# Pour ne pas s'embêter, on peut faire un GAM et il se débrouille avec les splines



#### Régression logistique
# Appli cours
rm(list=ls())
dat <- read.csv("chd.csv",sep=';')
summary(dat)

dat$tr_age <- cut(dat$age,breaks = quantile(dat$age,probs=seq(0,1,.2))  )

table(dat$chd,dat$tr_age) %>% prop.table(margin = 2)
log <- glm(chd ~ age,data=dat,family = binomial(link = "logit"))
summary(log)           

log <- glm(chd ~ tr_age,data=dat,family = binomial(link = "logit"))
summary(log)    
predict(log,select(dat,tr_age),type = "response")

# TD
dat <- read.table("spambase.data",sep=",")
#par(mfrow=c(2,3))
#apply(dat[,-58],MARGIN = 2,function(x) plot(density(x),main="",xlab="",ylab=""))

log <- glm(data=dat,formula = V58 ~ .,family = binomial)
choix <- regsubsets(select(dat,-V58),dat$V58,method = "forward")
#choix <- step(log)
plot(choix)
