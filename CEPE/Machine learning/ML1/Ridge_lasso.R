require(tidyverse)
require(mgcv)
require(glmnet)
require(plotly)
require(bestglm)
require(leaps)
require(ROCR)
require(pROC)
require(randomForest)
require(rpart)

setwd("/Volumes/USB/Datascience/ML1/")

dat <- read.table("ozone.txt",sep=";",na.strings = ".") %>% na.omit() %>% select(-date)

# Solution sans utilser na.strings
#dat$Ne18.2 <- ifelse(dat$Ne18==".","",as.character(dat$Ne18))  %>% as.numeric()

mco <- lm(maxO3~.,data=dat)
#resp <- residuals(reg,type = "partial")

x <- select(dat,-maxO3) %>% as.matrix()

ridge <- glmnet(x=x,y=dat$maxO3,alpha = 0,lambda = 0,standardize = T)
coefficients(ridge)
coefficients(mco)

ridge <- glmnet(x=x,y=dat$maxO3,alpha = .5,lambda = seq(0,100,10))
norme <- apply(ridge$beta,MARGIN = 2,function(x) t(x)%*%x)
norme <- data.frame((lambda=ridge$lambda),n=norme)
ggplot(norme,aes(lambda,n)) + geom_line() + ggtitle("Norme des coefficients")

beta <- ridge$beta %>% as.matrix() %>% data.frame()
names(beta) <- ridge$lambda
beta$var <- rownames(beta)
beta <- gather(beta,key = lambda,value = coeff,-var) %>% 
            mutate(lambda=as.numeric(substr(lambda,1,3)))

g <- ggplot(beta,aes(lambda,coeff,colour=var)) + geom_line() 
ggplotly(g)


## Solution plus rapide pour avoir le chemin de régularisation
matplot(ridge$lambda,t(ridge$beta),type="l")

# Détermination automatique du lambda
l_ridge <- cv.glmnet(x=x,y=dat$maxO3,alpha = 0)$lambda.1se
l_lasso <- cv.glmnet(x=x,y=dat$maxO3,alpha = 1)$lambda.1se
l_elast <- cv.glmnet(x=x,y=dat$maxO3,alpha = .5)$lambda.1se

## Validation croisée pour déterminer le meilleur lambda
set.seed(1234)
k <- 10 # nombre de blocs
groupe <- round(runif(nrow(dat),0,k))
# autre solution 
groupe <- sample((1:nrow(dat)) %% k +1 ) ## %% = modulo
table(groupe)

res <- matrix(0,nrow = nrow(dat),ncol=6)
colnames(res) <- c("vrai","MCO", "MCOch","lasso","ridge","elast")
res[,"vrai"] <- dat$maxO3
rescoeff <- array(0,dim=c(10,12,5))
for (ii in 1:k)
{
  # MCO 
  fit <- lm(maxO3~.,data=dat[groupe!=ii,])
  res[groupe==ii,"MCO"] <- predict(fit,dat[groupe==ii,])
  rescoeff[ii,,1] <- coefficients(fit)[-1] 
  # Ridge
  ll <- cv.glmnet(x=x[groupe!=ii,],y=dat$maxO3[groupe!=ii],alpha = 0)$lambda.1se
  fit <- glmnet(x=x[groupe!=ii,],y=maxO3[groupe!=ii],alpha = 0,lambda = ll)
  res[groupe==ii,"ridge"] <- predict(fit,x[groupe==ii,])
}
ii <- 1
### A compléter









###############################################
### On refait la même chose avec les spam

rm(list=ls())
setwd("/Volumes/USB/Datascience/ML1/")
dat <- read.table("spambase.data",sep=",") %>% mutate(V58=as.factor(V58))

set.seed(1234)
nbloc <- 10
bloc <- sample((1:nrow(dat))%%nbloc+1)
RES <- matrix(0,nrow=nrow(dat),ncol=7)
colnames(RES) <- c("vrai","MCO","lasso","ridge","elast","arbre","foret")
RES[,"vrai"] <- dat[,"V58"]
Llasso <- 1:nbloc
Lridge <- 1:nbloc
Lelast <- 1:nbloc
REScoef <- array(0,dim=c(nbloc,57,4))
XX <- model.matrix(V58~.,data=dat)



##################################
for(ii in 1:nbloc){
  print(ii)
  ##########MCO 
  mod <- glm(V58~.,data=dat[bloc!=ii,],family="binomial")
  RES[bloc==ii,"MCO"] <- predict(mod,dat[bloc==ii,],type="response")
  REScoef[ii,,1] <- coefficients(mod)[-1]
  ##########lasso
  mod <- cv.glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],family="binomial")
  ll <- mod$lambda.1se
  Llasso[ii] <- ll
  mod <- glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],lambda=ll,
                family="binomial")
  REScoef[ii,,2] <- coefficients(mod)[-c(1:2)]
  RES[bloc==ii,"lasso"] <- predict(mod,XX[bloc==ii,],type="response")
  ##########ridge
  mod <- cv.glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],alpha=0,
                   family="binomial")
  ll <- mod$lambda.1se
  Lridge[ii] <- ll
  mod <- glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],lambda=ll,alpha=0,
                family="binomial")
  REScoef[ii,,3] <- coefficients(mod)[-c(1:2)]
  RES[bloc==ii,"ridge"] <- predict(mod,XX[bloc==ii,],type="response")
  ##########elast
  mod <- cv.glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],alpha=0.5,
                   family="binomial")
  ll <- mod$lambda.1se
  Lelast[ii] <- ll
  mod <- glmnet(XX[bloc!=ii,],dat[bloc!=ii,"V58"],lambda=ll,
                alpha=0.5,family="binomial")
  REScoef[ii,,4] <- coefficients(mod)[-c(1:2)]
  RES[bloc==ii,"elast"] <- predict(mod,XX[bloc==ii,],type="response")
  
  ##########arbre
  mod <- rpart(formula = V58~.,data = dat[bloc!=ii,])
  RES[bloc==ii,"arbre"] <- predict(mod,dat[bloc==ii,]) [,1]
  
  ##########randomForest
  mod <- randomForest(formula = V58~.,data = dat[bloc!=ii,])
  RES[bloc==ii,"foret"] <- predict(mod,XX[bloc==ii,],type="prob")[,1]
}

apply(RES,2,FUN=function(x){mean((RES[,1]-x)^2)})
apply(RES,2,FUN=function(x){mean(abs(RES[,1]-x)/x*100)})
apply(RES,2,FUN=function(x){sum(abs(RES[,1]-x)>10)})
matplot(REScoef[,,3],type="l")

# Matrice de confusion
comptage <- function(x,seuil=.5,vrai)
{
  xx=vrai
  xx[x<=seuil] <- 1
  xx[x>seuil] <- 2
  table(xx,vrai)
}
comptage(RES[,2],vrai=RES[,1])
comptage(RES[,3],vrai=RES[,1])
comptage(RES[,4],vrai=RES[,1])

par (mfrow=c(2,3))
for (ii in 2:ncol(RES))
{
  rocglm <- roc(RES[,1],RES[,ii])
  plot(rocglm,main=paste(colnames(RES)[ii],round(auc(rocglm),3)))
}

