don <-  read.table("spambase.data",sep=",")
names(don)[58] <- "Y"
don$Y <- as.factor(don$Y)
###glm global
mod <- glm(Y~.,data=don,family="binomial")
###choix avec step
#choix <- step(mod)
###pour le récupérer
summary(choix)$terms
###########################################
nbbloc <- 10
llasso <- 1:nbbloc
lridge <- 1:nbbloc
lelast <- 1:nbbloc
bloc <- sample((1:nrow(don))%%nbbloc+1)
X <- model.matrix(Y~.,data=don)
RES <- data.frame(Y=don$Y,log=0,ridge=0,lasso=0,elast=0)
library(glmnet)
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- glm(Y~.,data=don[bloc!=ii,],family="binomial")
    prev <- predict(mod,don[bloc==ii,],type="response")
    RES[bloc==ii,"log"] <-  prev
#### lasso
    mod <- cv.glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=1,family="binomial")
    choix <- mod$lambda.1se
    llasso[ii] <- choix
    mod <- glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=1,lambda=choix,
                  family="binomial")
    prev <- predict(mod,X[bloc==ii,],type="response")
    RES[bloc==ii,"lasso"] <-  prev
#### ridge
    mod <- cv.glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=0,family="binomial")
    choix <- mod$lambda.1se
    lridge[ii] <- choix
    mod <- glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=0,lambda=choix
                 ,family="binomial")
    prev <- predict(mod,X[bloc==ii,],type="response")
    RES[bloc==ii,"ridge"] <-  prev
#### elast
    mod <- cv.glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=.5,family="binomial")
    choix <- mod$lambda.1se
    lelast[ii] <- choix
    mod <- glmnet(X[bloc!=ii,],don[bloc!=ii,"Y"],alpha=.5,lambda=choix
                 ,family="binomial")
    prev <- predict(mod,X[bloc==ii,],type="response")
    RES[bloc==ii,"elast"] <-  prev
}

RES$arbre <- 0
RES$foret <- 0
library(rpart)
library(randomForest)
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- rpart(Y~.,data=don[bloc!=ii,])
    prev <- predict(mod,don[bloc==ii,],type="prob")[,2]
    RES[bloc==ii,"arbre"] <-  prev
####
    mod <- randomForest(Y~.,data=don[bloc!=ii,],ntree=100)
    prev <- predict(mod,don[bloc==ii,],type="prob")[,2]
    RES[bloc==ii,"foret"] <-  prev
}

RES$foret500 <- 0
RES$foret100d1 <- 0
RES$foret100d57 <- 0
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- randomForest(Y~.,data=don[bloc!=ii,],ntree=500)
    prev <- predict(mod,don[bloc==ii,],type="prob")[,2]
    RES[bloc==ii,"foret500"] <-  prev
####
    mod <- randomForest(Y~.,data=don[bloc!=ii,],ntree=100,mtry=1)
    prev <- predict(mod,don[bloc==ii,],type="prob")[,2]
    RES[bloc==ii,"foret100d1"] <-  prev
####
    mod <- randomForest(Y~.,data=don[bloc!=ii,],ntree=100,mtry=57)
    prev <- predict(mod,don[bloc==ii,],type="prob")[,2]
    RES[bloc==ii,"foret100d57"] <-  prev
}

dim(RES)
resultat <- (2:ncol(RES))*0
names(resultat) <- colnames(RES)[-1]

library(pROC)
par(mfrow=c(3,3))
for(ii in 2:10){
  result.roc <- roc(RES[,"Y"],RES[,ii])
  res.auc <- round(pROC::auc(result.roc),2)
  resultat[ii-1] <- res.auc
  plot(result.roc,col=ii,
       main=paste(colnames(RES)[ii],"auc=",res.auc))
}
RES$moy <- (RES$foret+RES$ridge)/2
sort(resultat,dec=T)
library(e1071)
RES$svm1 <- 0
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- svm(Y~.,data=don,kernel="linear",cost=1,type="C-classification",
               probability=TRUE)
    prev <- predict(mod,don[bloc==ii,],probability=TRUE)
    RES[bloc==ii,"svm1"] <-  prev
}
library(kernlab)
RES$ksvm1 <- 0
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- ksvm(Y~.,data=don,kernel="vanilladot",C=1)
    prev <- predict(mod,don[bloc==ii,])
    RES[bloc==ii,"ksvm1"] <-  prev
}


###GBM
RES$gbmADA <- 0
RES$gbmBE <- 0
for(ii in 1:nbbloc){
    set.seed(1234+ii)
    print(ii)
####
    mod <- gbm(Y~.,data=don[bloc!=ii,],distribution="adaboost",
             interaction.depth=2,shrinkage=0.05,
             train.fraction=0.75,n.trees=500)
    iter <- gbm.perf(mod)
    mod <- gbm(Y~.,data=don[bloc!=ii,],distribution="adaboost",
             interaction.depth=2,shrinkage=0.05,n.trees=iter)
    prev <- predict(mod,don[bloc==ii,],type="response",n.trees=iter)
    RES[bloc==ii,"gbmADA"] <-  prev
####
    mod <- gbm(Y~.,data=don[bloc!=ii,],distribution="bernoulli",
             interaction.depth=2,shrinkage=0.05,
             train.fraction=0.75,n.trees=500)
    iter <- gbm.perf(mod)
    mod <- gbm(Y~.,data=don[bloc!=ii,],distribution="bernoulli",
             interaction.depth=2,shrinkage=0.05,n.trees=iter)
    prev <- predict(mod,don[bloc==ii,],type="response",n.trees=iter)
    RES[bloc==ii,"gbmBE"] <-  prev
}




















library(pROC)
par(mfrow=c(2,3))
for(ii in 2:7){
    result.roc <- roc(RES[,"Y"],RES[,ii])
    res.auc <- round(pROC::auc(result.roc),2)
    plot(result.roc,col=ii,
         main=paste(colnames(RES)[ii],"auc=",res.auc))
}

####ANALYSE FORET
ii=1
mod <- randomForest(Y~.,data=don[bloc!=ii,],ntree=100)
plot(mod) #pour visualiser les erreurs
mod$importance #pour voir les variables importantes
###JOUER avec ntree = nbr d'arbres
###      et   mtry  = nbr de variables sélectionnées


####ANALYSE SVM avant de rajouter dans les boucles
ii=1
library(e1071)
tune.out <- tune(svm,Y~.,data=don[bloc!=ii,],kernel="linear",
                 ranges=list(cost=c(0.001,0.01,1,10,100,1000)))
summary(tune.out)
bestmod <- tune.out$best.model

library(gbm)
model_ada <- gbm(Y~.,data=don,distribution="adaboost",
                 interaction.depth=2,shrinkage=0.1,n.trees=500)

model <- gbm(Y~.,data=don,distribution="adaboost",
             interaction.depth=2,shrinkage=0.05,
             train.fraction=0.75,n.trees=500)
gbm.perf(model,method="test")


model_logit <- gbm(Y~.,data=don,distribution="bernoulli",
                   interaction.depth=2,shrinkage=0.1,train.fraction=0.75,
                   n.trees=500)

gbm.perf(model_logit,method="test")
