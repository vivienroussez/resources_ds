---
title: "Convex Optimization"
header-includes:
   - \usepackage{bbm, mathrsfs}
   - \usepackage{algorithm2e}
   - \usepackage{amsmath,amsfonts,amssymb}
output: html_document
---

# Introduction

```{r child = 'CVX1--Intro.Rmd'}
```


# Numerical linear algebra
```{r child = 'CVX6-Numerical linear algebra.Rmd'}
```


# Stats ML And Optimization
```{r child = 'CVX7-StatsMLAndOptimization.Rmd'}
```


# Applications

```{r child = 'CVX1.1-Applications-light.Rmd'}
```


# Sets
```{r child = 'CVX2-Sets.Rmd'}
```

  
# Functions
```{r child = 'CVX3-Functions.Rmd'}
```


# Problems
```{r child = 'CVX4-Problems.Rmd'}
```


# Duality
```{r child = 'CVX5-Duality.Rmd'}
```
