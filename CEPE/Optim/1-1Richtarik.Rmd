---
title: "Richtarik"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(knitr)
library(DT)
library(xtable)
```


## Prediction	of	Object	Labels

| Set of “natural” objectsA | Set of labelsB                | Prediction task             |
|---------------------------|-------------------------------|----------------------------|
| NYT,articles              | Article,category,(finite,set) | Multi-class,classification |
| E-mails                   | Spam,/,not-spam               | Binary classification      |
| Images                    | Image category,(finite,set)   | Multi-class,classification |
| Surveillance,videos       | Probability,of,a,threat       | Regression                 |
| User,clicks               | Age                           | Regression                 |



## Statistical	Model	of	Objects	&	Labels


<img src="img/kt.png" alt="Drawing" style="width: 450px;"/>

We	assume	that	object-label	pairs	occur	in	nature	according	to	
some	(unknown)	distribution:

Given	a	sampled	object	$a_i$ predict	the	unknown	label $b_i$


|                  | Input Space     | Feature,map            |
|------------------|-----------------|------------------------|
| Linear Predictor | $x^T \Phi(a_i)$ | $\Phi(a_i)$            |
| Neural Network   |                 | non linear : <img src="img/neuralmodel.png"/>
 combination |



##  Predictor :  

$h_x : A \rightarrow \mathbb{R}$ with  $x \in \mathbb{R}^d$

## Loss	and	Expected	Loss

$$loss(h_x(a_i), b_i)$$

## Expected	Risk	Minimization

$$\text{min}_{x ^in \mathbb{R}^d} \mathbf{E}_{(a_i, b_i) \sim  D}  [loss(h_x(a_i), b_i) ]$$

## Empirical	Risk

* Our Output	predictor	which	minimizes	the	Empirical	Risk

$$\text{min}_{x ^in \mathbb{R}^d} \frac{1}{n} \sum_{i = 1}^n  [loss(h_x(a_i), b_i) + g(x)]$$ 
$$\text{min}_{x ^in \mathbb{R}^d} \frac{1}{n} \sum_{i = 1}^n  [f_i(a_i^T, x, b_i) + g(x)] $$

## Examples	of	ERM	Problems

<img src="img/erm.png" alt="Drawing" style="width: 450px;"/>


## Interesting	Classes	of	ERM	Problems	 Based	on	Dimensions


<img src="img/dimensions.png" alt="Drawing" style="width: 450px;"/>


## History

<img src="img/history.png" alt="Drawing" style="width: 450px;"/>


* Gradient	Descent	(1847) “Just	follow	a	ball	rolling	 down	the	hill”





