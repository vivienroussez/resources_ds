Minimize
cost: q1 + 3q2 + 16q3 + 3q4
Subject To
d1: 3q1 + 4q2 -3q3 + q4 = 2
d2: 3q1 - 2q2 + 6q3 - q4 = 1
d3: 6q1 +4q2 + q4 = 4
Bounds
0 <= q1 
0 <= q2 
0 <= q3 
0 <= q4 
End