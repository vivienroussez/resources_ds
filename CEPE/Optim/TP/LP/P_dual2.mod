Minimize
cost: 6q1 + 4q2
Subject To
d1: 3q1 + 2q2 >= 3
d2: 2q1 + 4q2 >= 4
Bounds
q1 >= 0 
q2 >= 0
End