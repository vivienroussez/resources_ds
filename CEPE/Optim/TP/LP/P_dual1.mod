Maximize
cost: 2q1 + q2 + 4q3
Subject To
d1: 3q1 + 3q2 + 6q3 <=1
d2: 4q1 - 2q2 + 4q3 <= 3 
d3: -3q1 +6q2   <= 3
d4: q1 -q2 + q3 <= 1
End