# The Diet Problem with lpSolve
setwd("TP/LP")

# Filenames for data and constraints
dataFileName <- "dataFiles_DietProblemData_nut.csv"
constraintsFileName <- "dataFiles_DietProblemData_bound.csv"

# data layout information
dataColumns <- 11  # number of decision variables
costColumn <- 12

# Set up data - nutrition table in the Diet Problem example
data <- read.csv2(dataFileName, sep = ";", stringsAsFactors = F)
rownames(data) <- data$Food
data$Food <- NULL
data$Index <- NULL
A <- as.matrix(data[, -costColumn])

# the right-most column is cost per unit, e.g.'Price/serving ($)'
Cost <- as.matrix(data[, costColumn, drop = FALSE])

# Set up constraints - e.g. max/min for each nutrient type
boundaries <- read.csv2(constraintsFileName, sep = ";",  stringsAsFactors = F)
b <- c(boundaries$Min, boundaries$Max)

library(lpSolve)

# Run the solver
res <- lp(objective.in = Cost, 
      const.mat = rbind(t(A), t(A)), 
      const.dir = c(rep(">=", nrow(boundaries)), 
                    rep("<=", nrow(boundaries))), 
      const.rhs = b)

amount <- res$solution
names(amount) <- rownames(data)
amount[amount != 0]

res$objval
