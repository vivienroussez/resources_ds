Maximize
cost: 3q1 + 4q2
Subject To
d1: 3q1 + q2 <= 6
d2: 2q1 + 4q2 <= 4
End