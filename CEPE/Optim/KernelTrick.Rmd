---
title: "Optimization Methods"
output: html_document
---

## Supervised Learning

## Misc 

### the kernel trick : 

#### Problem :  Dealing with Nonseparable Data 

http://www.eric-kim.net/eric-kim-net/posts/1/kernel_trick.html

<img src="img/dataset_nonsep.png" alt="Drawing" style="width: 450px;"/>

* the primary obstacle is the constraint that the decision boundary be linear in the original input space (here $\mathbf{R}^2$)

!["2 -> 3"](img/data_2d_to_3d.png)

* In the $\mathbf{R}^3$ space, we can train a linear SVM classifier that successfully finds a good decision boundary. 

* Challenge is to find a transformation $\Phi : \mathbf{R}^2 \rightarrow \mathbf{R}^3$ , such that the transformed dataset is linearly separable in $\mathbf{R}^3$

!["2 -> 3"](img/data_2d_to_3d_hyperplane.png)

* A dataset that is not linearly separable in $\mathbf{R}^N$ may be linearly separable in a higher-dimensional space $\mathbf{R}^M$ (where M > N) hence the $\Phi\ transformation

##### large dimensions : computational consequences of increasing the dimensionality

##### But We only need the dot products! :

* During training, the optimization problem only uses the training examples to compute pair-wise dot products $(X_i,X_j)$ , where $X_i,X_j \in \mathbf{R}^N$. 

* The use of the inner (dot)  product instead of the data vectors themselves will yield great computational savings and allow kernelization.

* Kernel functions computes the dot product between v and w in $\mathbf{R}^N$ a higher-dimensional without explicitly transforming v and w to $\mathbf{R}^N$.

* a Kernel function is a function $K : \mathbf{R}^N \times \mathbf{R}^N  \rightarrow \mathbf{R}$

We can implicitly transform datasets to a higher-dimensional using no extra memory, and with a minimal effect on computation time. 

The advantage of  a kernel function is that the complexity of the optimisation problem remains only dependent on the dimensionality of the input space and not of the feature space.



