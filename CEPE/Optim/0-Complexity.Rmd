---
title: "Complexité des algorithmes"
output: html_document
---

L'analyse de la complexité d'un algorithme consiste en l'étude formelle de la quantité de ressources (par exemple de temps ou d'espace) nécessaire à l'exécution de cet algorithme.

On va souvent calculer le temps de calcul dans le pire des cas

Par exemple pour la recherche linéaire vs dichotomique d'un élément dans une liste de taille N

+ Au pire des cas on mettra N opérations pour la recherche linéaire
+  Dans le cas dichotomiqe : le nombre d'étapes nécessaire sera le nombre entier qui est immédiatement plus grand que   ${\displaystyle \log _{2}\,n}$ (15 pour 30 000)


## DL

Un développement limité (noté DL) d'une fonction en un point, est une approximation polynomiale de cette fonction au voisinage de ce point,

$$f(x) = f(a) + (x-a)f'(a) + o(x-a)$$


## Notation o : Petit o 

On écrit $v=o(u)$  Si $\lim\limits_{n \rightarrow +\infty} \frac{u(n)}{v(n)} = 0$

On dit encore dans ce cas que v est *négligeable devant u* de facon asymptotique.

Le petit-o est courant en mathématiques mais plus rare en informatique



## Notation O : Grand O

La notation grand O de Landau dénote le caractère dominé d'une fonction par rapport à une autre.

On écrit $v=O(u)$ ou $v(n)=O(u(n))$ s'il existe une constante positive $K$ telle que pour $n$ suffisamment grand $|v(n)|≤Ku(n)$. 

Soit encore: $∃ N | n≥N ⇒ |v(n)|≤K|u(n)|.$

La fonction/série v est bornée par la fonction u asymptotiquement, à un facteur près

Exemples :

* La suite $v_n=(2n^2+1)/n$ est $O(n)$ .  En effet, le rapport $\frac{v_n}{n}$ tend vers 2, donc pour n suffisamment grand on a $v_n≤3n$.

* Si $v$ et $w$ sont $O(u)$ alors $u+w$ est aussi $O(u)$.
* Si v est $O(u)$ et si $\lambda$  est une constante, $\lambda v$ est également $O(u)$.
* $o⊂O$

## Complexité par ordre croissant

La variable (notée ici n) tend vers $+\infty$ et c est une constante réelle arbitraire.

* $O(1)$ constante
* $O(log(n))$ logarithmique
* $O((log(n))c)$ polylogarithmique
* $O(n)$ linéaire
* $O(nlog(n))$ parfois appelée « linéarithmique », ou « quasi-linéaire »
* $O(n^2)$ quadratique
* $O(n^c)$ polynomiale
* $O(c^n)$ exponentielle , parfois « géométrique »
* $O(n!)$ factorielle

Pour avoir une idée, sur des problèmes d'affectation (ranger des convives à une table, affecter un taxi à un client  ...) on est en $O(n^4)$ (parfois $O(n^3)$)


https://fr.wikipedia.org/wiki/Analyse_de_la_complexit%C3%A9_des_algorithmes

http://discrete.gr/complexity/
