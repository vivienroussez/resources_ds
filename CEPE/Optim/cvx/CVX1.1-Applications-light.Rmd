--
title: "Applications"
output: html_document
---

## Applications
 
### Corporate Finance


* Capital Budgeting:  Choose a combination of capital projects to maximize overall  NPV (Net Present Value)
 

* Capacity Planning:  Determine which plants should be opened or closed

 
### Investments

 

* Portfolio Optimization   Allocate funds to stocks to minimize risk for a target rate of return - with known or computed variances and covariances
 
 
###  Production

* Machine Allocation:  Allocate production of a product to different machines, with different capacities, startup cost and operating cost, to meet production target at minimum cost
  
* Blending:  Determine which raw materials from different sources to blend to produce a substance with certain desired qualities at minimum cost
  
 
### Distribution

 

* Transportation Model:  Determine how many products to ship from each factory to each warehouse, or from each factory to each warehouse and direct to each end customer, to minimize shipping cost while meeting warehouse demands and not exceeding factory supplies
  
 
 
### Human Resources


  
* Office Assignment:  Assign employees to available offices to maximize satisfaction of employee preferences
  
* Workforce Movement:  Decide how many troops to move from several camps to several other bases, to minimize movement time or total cost

 
### Transportation

* Revenue Management:  For different classes of tickets, determine how many seats to sell or hold back as flight date approaches

 
### Oil and Gas

 

* Gasoline Blending: From hydrocarbons with specific octane ratings, vapor pressure, volatility and cost, determine how much of each should be blended together to produce regular, midgrade, and premium gasoline
  

 
### Agriculture

* Crop Planning:  Given forecasted crop prices and growing conditions, determine how much of each crop to plant

* Feed Blending:  Given the nutritional requirements for feed animals and the price of available feeds, find the blend of feed ingredients that will minimize total cost

 
### Electric Power

* Generator Commitment:  Given forecasted demand by period and operating cost for each generator, determine which generators should be run in each time interval
 


 
 