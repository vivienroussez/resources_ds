---
title: "Gradient Algorithms"
output: html_document
---
# Examples 

###  Linear regression by minimising the residual sum of square 

Consider the function
min.RSS : : $y = par[1] + par[2] * x.$  par are the model parameters. 


min.RSS is a function that calculates the residual sum of square 

* Use optim or optimize functions to minimise the min.RSS with regard to the following data 

```{r}
dat=data.frame(x=c(1,2,3,4,5,6), 
               y=c(1,3,5,6,8,12))
```

* Use lm to calculate the regression of x against y : Compare results

* Plot y~x  and regression lines


#### For two varables:   With quadprog  $RSS = ( Y - X b )' ( Y - X b )$

$RSS = ( Y - X b )' ( Y - X b)  = Y Y' - 2 Y' X b + b X' X b$

```{r}

n <- 100
x1 <- rnorm(n)
x2 <- rnorm(n)
y <- 1 + x1 + x2 + rnorm(n)
X <- cbind( rep(1,n), x1, x2 )

```

* use lm and solve.QP to calculate the regression of $x_1 + x_2$  against $y$

### Maximum likelihood

Maximum likelihood estimation (MLE) is a method to  estimate the parameters of a statistical model given observations, by finding the parameter values that maximize the likelihood of making the observations given the parameters

Given a statistical model, all of the evidence in a sample relevant to model parameters is contained in the likelihood function.

How to fit a Poisson distribution to data ? 

```{r}
obs = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17, 42, 43)
freq = c(1392, 1711, 914, 468, 306, 192, 96, 56, 35, 17, 15, 6, 2, 2, 1, 1)
x <- rep(obs, freq)
plot(table(x), main="Count data")

```


Maximise the likelihood for the chosen parameter lambda.

```{r}

poisson <- function(x, lambda) lambda^x/factorial(x) * exp(-lambda)

log.poisson <- function(x, lambda){ 
                     -sum(x * log(lambda) - log(factorial(x)) - lambda) 
                     }

res <- optim(par = 2, log.poisson, x = x)

```


```{r}

res <-optim(par = 2, log.poisson, x = x, method = "Brent", lower = 2, upper = 3)
res

y <- lapply(x, log.poisson, lambda = res$par)
plot(x, y)

res <-optimize(log.poisson, x = x, lower = 2, upper = 3)
res
```



# Algorithms

## Gradient descent


Newton computes the update step s by solving $F′(x)·s=−F(x)$

Gauss-Newton determines the update by minimizing the error in the linearization of the overdetermined system, i.e., minimizes $‖F′(x)·s+F(x)‖$ . The expanded form of the square of this error is $‖F(x)‖^2+2F(x)^TF′(x)·s+s^T·F′(x)^TF′(x)s$

The quadratic term is not an approximation for the Hessian of ‖F(x+s)‖2, just an expression in the error minimization of a linear system.


The problem of finding the minimum of a function f(x) can be interpreted geometrically as follows: if f represents the altitude in a landscape, minimizing f means finding the lowest valley. A straightforward algorithm comes to mind: just go downhill.

<img src="img/img30.png?raw=true" />


* Start with an initial guess x of the solution
* Compute the derivative (also called "gradient") of f to find the direction of the steepest (downwards) slope
* Take a step in this direction
* Stop when you can no longer find a downwards direction. 

You have to choose the step size beforehand, but you may want it to depend on the norm of the gradient.

## Nelder-Mead

If you do not know the derivative of the function (or if it not not differentiable)

* Start with an initial guess x.  
* Choose a direction at random, among the n coordinates, 
* step forwards and backwards in this direction, and compare the values of f: f(x), f(x+h), f(x-h). If one of the new values is better than the current one, move to that point; otherwise do not move. 
* Iterate until convergence.

The algorithm can be improved by changing the step size: increase it (say, by 10%) when you can move, decrease it when you do not (because when you cannot move, the minimum is probably near and you may be stepping over it), and stop when the step size is too small. 

You can use a different step size for each coordinate, to prevent problems if their scales are different.

The points x+h, x-h, around x in all directions, form a kind of (square) "ball": the algorithm just lets the ball roll downhill, and uses a smaller ball, for better precision, when it gets stuck over a valley too small for it to explore.

In dimension n, that square ball has 2*n points (the hypercube has 2^n vertices, but we only use the points at the center of the facets). The Nelder-Mead algorithm uses a similar idea, but with a slightly more economical triangular (tetrahedral) ball: it only has n+1 points.

## Stochastic gradient


$$Q(w) = \sum_{i=1}^n Q_i(w)$$

Standard Gradient descent iterates like : 
$$w := w - \eta \nabla Q(w) = w - \eta \sum_{i=1}^n \nabla Q_i(w),$$ 

Evaluating the sums of gradients becomes very expensive as number of examples increases 


Stochastic approximations, maintain one iterate as a parameter estimate,  and successively update that iterate based on a single data point.  When the update is based on a noisy gradient, the stochastic approximation is known as standard stochastic gradient descent , which has been fundamental in modern applications with large data sets. 


In stochastic (or "on-line") gradient descent, the true gradient of $Q ( w )$  is approximated by a gradient at a single example


* Choose an initial vector of parameters w and learning rate $\eta$ .
* Repeat until an approximate minimum is obtained:
    + Randomly shuffle examples in the training set.
    + For $i=1,2,...,n$, do:
            $w:=w-\eta \nabla Q_{i}(w)$

A compromise between computing the true gradient and the gradient at a single example, is to compute the gradient against more than one training example (called a "mini-batch") at each step.

Process the observations one by one ("online" algorithm): compute the gradient of the term corresponting to the current observation, move in its direction (use a smaller step size than you would for the full gradient), and continue with the next term.

## Conjugate gradient

Gradient descent sometimes oscillates around the solution, or moves in zig-zag before reaching the optimum. 

This can be remedied by moving, not in the direction of the gradient, but in the direction of some linear combination of the gradient and the previous direction. 

This can be interpreted as a kind of inertia: we turn partly, but not completely, in the direction of the gradient,

## The Hessian and its approximations

The gradient descent algorithm only used the first derivative (i.e., approximated the function to minimize by a straight line). 

We can approximate the function by its second Taylor expansion: the approximation is a quadratic function and (provided it is positive definite), it has an extremum. This requires the second derivative (Hessian matrix) and works reasonably well in low dimensions. It is actually Newton's method, used to solve $f'(x) = 0$.

P roblem with the hessian:  size. If there are n parameters to estimate, the hessian is an n*n matrix -- when n is large, this is unmanageable. 

We need to find an approximation of this matrix that does not require the computation and storage of n^2 values.

* If the function to minimize is a sum of squares, the computations can be simplified by approximating the hessian (Gauss-Newton): 

  
```{r eval=F}
h = - ( grad(r) . grad(r) ) ^ -1 * grad(r) . r

```

To avoid problems when inverting the matrix, the Levenberg-Marquardt algorithm shrinks it towards the identity (this is similar to ridge regression):

```{r eval=F}
h = - ( grad(r) . grad(r) + lambda * I ) ^ -1 * grad(r) . r

```


In the general case, to avoid computing, storing and inverting the Hessian, we want a matrix B such that

```{r eval=F}
f'(x+h) ≈ f'(x) + B h.

```


There are many possible choices. One typically starts with an initial extimate (say, $B_0=I$), and updates an estimate of the hessian as the algorithm progresses towards the extremum, by adding an easy-to-compute matrix, e.g., a matrix of rank 1 (matrices of rank 1 can be written as the product of two vectors ab'). 


* For instance, the BFGS algorithm uses two matrices of rank 1:

```{r eval=F}
y = f'(x_{k+1}) - f'(x_k)
s =    x_{k+1}  -    x_k
B <- B + y y' / (y' s) - B s s' B / (s' B s).
```

It is possible to keep track of an approximation of the inverse of B at the same time.

* The L-BFGS (low-memory BFGS) algorithm only keeps the last L updates: there are only 2L vectors to store.   
  